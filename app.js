// let t = new Date("2012", "1", "15", "12")
// console.log(t)
// console.log(t.getTime())
// console.log(t.getFullYear())
// console.log(t.getDay())
// console.log(t.getDate())
// console.log(t.getHours())
// console.log(t.getUTCHours())

const days = ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", 
                "ხუთშაბათი", "პარასკევი", "შაბათი"]

function show_day(){
    const months = ["იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი",
                  "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი",
                  "ოქტომბერი", "ნოემბერი", "დეკემბერი"]            
    // days[2], days[5]            
    let d_res = document.getElementById("show-result");
    let t = new Date(); 
    d_res.innerHTML = "დღეს არის "+days[t.getDay()]
    let d_res_month = document.getElementById("show-result-months");
    d_res_month.innerHTML = t.getDate()+" "+months[t.getMonth()]
}

function show_f_day(){
    let d_f_res = document.getElementById("f-result");
    let t = new Date(); 
    let n = document.getElementById("d-number")
    let n_index = (t.getDay()+parseInt(n.value)) % 7
    d_f_res.innerHTML = n.value+" დღის შემდეგ იქნება "+days[n_index]
}